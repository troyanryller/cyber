class StatisticSerializer
  @serialize = (data) ->
    return {
      statusesBarChartData: @statusInfoSerialize(data.status_info)
      durationBarChartData: @durationInfoSerialize(data.duration_info)
    }

  @statusInfoSerialize = (statusInfoData) ->
    days = Object.keys(statusInfoData)
    passed = []
    failed = []
    errored = []
    stopped = []
    passedColours = []
    failedColours = []
    erroredColours = []
    stoppedColours = []
    abnormalDays = []
    days.forEach (e) ->
      if statusInfoData[e].abnormal
        abnormalDays.push(e)
        passedColours.push('rgb(67, 255, 0)')
        failedColours.push('rgb(255, 0, 0)')
        erroredColours.push('rgb(255, 0, 250)')
        stoppedColours.push('rgb(176, 142, 242)')
      else
        passedColours.push('rgb(28, 81, 9)')
        failedColours.push('rgb(136, 6, 6)')
        erroredColours.push('rgb(140, 18, 138)')
        stoppedColours.push('rgb(106, 102, 114)')
      passed.push(statusInfoData[e].passed || 0)
      errored.push(statusInfoData[e].error || 0)
      stopped.push(statusInfoData[e].stopped || 0)
      failed.push(statusInfoData[e].failed || 0)

    return {
      data:
        labels: days
        datasets: [{
          label: 'Passed'
          backgroundColor: passedColours
          data: passed
        }, {
          label: 'Errored'
          backgroundColor: erroredColours
          data: errored
        }, {
          label: 'Stopped'
          backgroundColor: stoppedColours
          data: stopped
        }, {
          label: 'Failed'
          backgroundColor: failedColours
          data: failed
        }]
      scales:
        xAxes: [{
          stacked: true,
          ticks:
            callback: (value, index, values) ->
              if abnormalDays.includes(value)
                '@' + value + '@'
              else
                value
          }]
        yAxes: [ { stacked: true } ]
    }

  @durationInfoSerialize = (durationInfoData) ->
    return {
      data:
        labels: durationInfoData.map (e) -> e[0]
        datasets: [{
          label: 'Duration'
          backgroundColor: 'green'
          fill: false
          data: durationInfoData.map (e) -> e[1]
        }]
    }

window.StatisticSerializer = StatisticSerializer
