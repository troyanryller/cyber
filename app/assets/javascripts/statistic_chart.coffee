class StatisticChart
  @build = ({element, data, title, type, scales}) ->
    return new Chart(element,
      type: type || 'bar'
      data: data
      options:
        title:
          display: true
          text: title

        tooltips:
          mode: 'index'
          intersect: false
        responsive: true
        maintainAspectRatio: false
        scales: scales || {xAxes: [{stacked: true}], yAxes: [{stacked: true}]}
    )

window.StatisticChart = StatisticChart
