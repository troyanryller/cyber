@loadCharts = (file) ->
  data = new FormData()
  data.append('file', file) if file
  data.append('k', document.getElementById('k').value)
  path = fileSelector.getAttribute('path')
  fetch path,
    method: 'POST'
    body: data
    headers:
      'X-Requested-With': 'XMLHttpRequest'
      'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    credentials: 'same-origin'
  .then (response) -> response.json()
  .then (data) -> StatisticSerializer.serialize(data)
  .then (serializedData) ->
    StatisticChart.build
      title: 'Builds status'
      data: serializedData.statusesBarChartData.data
      scales: serializedData.statusesBarChartData.scales
      element: buildStatusChart
    StatisticChart.build
      title: "Builds duration"
      type: 'line'
      data: serializedData.durationBarChartData.data
      element: durationChart


window.addEventListener 'load', ->
  @loadCharts()
  fileSelector.addEventListener 'change', => @loadCharts(fileSelector.files[0])
  document.getElementById('k').addEventListener 'change', => @loadCharts(fileSelector.files[0])
