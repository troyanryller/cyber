class BuilderSessionsController < ApplicationController
  def parse_file
    fetch_parsed_data
    render json: { status_info: fetch_status_info, duration_info: fetch_duration_info }
  end

  private

  def fetch_parsed_data
    # Can make some main parser which will check file type and delagate parsing to correct parser.
    @parsed_data = ::BuilderSession::CsvParser.new(file: file_path).call
  end

  def file_path
    params[:file]&.path || 'test/fixtures/files/session_history.csv'
  end

  def fetch_status_info
    ::BuilderSession::StatusInfoService.call(collection: @parsed_data, k: params[:k])
  end

  def fetch_duration_info
    ::BuilderSession::DurationInfoService.call(collection: @parsed_data)
  end
end
