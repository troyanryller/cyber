# frozen_string_literal: true
module Concerns
  module ActsAsStatic
    extend ActiveSupport::Concern

    module ClassMethods
      def call(params = {})
        new(params).call
      end

      def call!(params = {})
        new(params).call!
      end
    end
  end
end
