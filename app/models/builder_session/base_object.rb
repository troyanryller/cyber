module BuilderSession
  class BaseObject
    include Virtus.model

    class << self
      def by_date(collection)
        collection.group_by{ |e| e.created_at.to_date }
      end

      def by_date_with_status(collection)
        collection.group_by{ |e| [e.created_at.to_date, e.summary_status] }
      end
    end

    attribute :session_id, String
    attribute :started_by, String
    attribute :created_at, DateTime
    attribute :summary_status, String
    attribute :duration, Float
    attribute :worker_time, Float
    attribute :bundle_time, Float
    attribute :num_workers, Integer
    attribute :branch, String
    attribute :commit_id, String
    attribute :started_tests_count, Integer
    attribute :passed_tests_count, Integer
    attribute :failed_tests_count, Integer
    attribute :pending_tests_count, Integer
    attribute :skipped_tests_count, Integer
    attribute :error_tests_count, Integer
  end
end
