module BuilderSession
  class DurationInfoService
    include Virtus.model
    include ::Concerns::ActsAsStatic

    attribute :collection, Array[::BuilderSession::BaseObject]
    attribute :normalized_data, Array, lazy: true, default: :fetch_normalized_data

    def call
      normalized_data
    end

    private

    def fetch_normalized_data
      collection.reject { |e| e.summary_status != 'passed' }
                .sort { |e| e.created_at }
                .map { |e| [format_date(e), e.duration] }
    end

    def format_date(obj)
      obj.created_at.strftime("%d/%m %H:%M:%S")
    end
  end
end
