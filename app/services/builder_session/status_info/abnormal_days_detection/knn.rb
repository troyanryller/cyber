module BuilderSession
  module StatusInfo
    module AbnormalDaysDetection
      class Knn
        include Virtus.model
        include ::Concerns::ActsAsStatic

        attribute :data, Hash
        attribute :k, Integer
        attribute :common, Float, lazy: true, default: :fetch_common

        def call
          data.map { |k,v| [k, (v - common).abs] }
              .sort_by { |_,v| v }
              .take(k)
              .map(&:first)
        end

        private

        def fetch_common
          data.values.max
        end
      end
    end
  end
end
