module BuilderSession
  module StatusInfo
    module AbnormalDaysDetection
      class ToPercentageNormalizer
        DEFAULT_DAY_AVERAGE_PERCENTAGE_BUFFER = 0.9
        include Virtus.model
        include ::Concerns::ActsAsStatic

        attribute :data, Hash
        attribute :percentage_buffer, Float, default: DEFAULT_DAY_AVERAGE_PERCENTAGE_BUFFER

        attribute :total_builds, Float, lazy: true, default: :fetch_total_builds
        attribute :day_average, Float, lazy: true, default: :fetch_day_average
        attribute :fails_percentage, Hash, lazy: true, default: :fetch_fails_percentage

        def call
          fails_percentage
        end

        private

        def fetch_total_builds
          data.values.sum { |e| e[:total] }
        end

        def fetch_day_average
          total_builds / data.count
        end

        def fetch_fails_percentage
          data.select { |_, info| info[:total] >= (day_average * percentage_buffer) }
              .transform_values { |e| e[:failed].to_f / e[:total] }
        end
      end
    end
  end
end
