module BuilderSession
  module StatusInfo
    module AbnormalDaysDetection
      class Detector
        include Virtus.model
        include ::Concerns::ActsAsStatic

        attribute :data, Hash
        attribute :k, Integer

        attribute :abnormal_days, Array, lazy: true, default: :fetch_abnormal_days
        attribute :normalized_abnormal_hash, Hash, lazy: true, default: :fetch_normalized_abnormal_hash
        attribute :data_normalizer, Object, lazy: true,
                  default: ->(*) { ::BuilderSession::StatusInfo::AbnormalDaysDetection::ToPercentageNormalizer }
        attribute :anomaly_finder, Object, lazy: true,
                  default: ->(*) { ::BuilderSession::StatusInfo::AbnormalDaysDetection::Knn }

        def call
          data.deep_merge(normalized_abnormal_hash)
        end

        private

        def fetch_abnormal_days
          anomaly_finder.call(data: data_normalizer.call(data: data), k: k)
        end

        def fetch_normalized_abnormal_hash
          abnormal_days.map { |e| [e, {abnormal: true}] }.to_h
        end
      end
    end
  end
end
