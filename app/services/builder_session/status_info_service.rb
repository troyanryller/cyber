module BuilderSession
  class StatusInfoService
    include Virtus.model
    include ::Concerns::ActsAsStatic

    attribute :collection, Array[::BuilderSession::BaseObject]
    attribute :common, Float, default: 4
    attribute :k, Integer, default: 3

    attribute :sorted_collection, Array[::BuilderSession::BaseObject], lazy: true, default: :sort_collection
    attribute :count_data, Hash, lazy: true, default: :fetch_count_data
    attribute :normalized_data, Hash, lazy: true, default: :fetch_normalized_data

    def call
      normalized_data
    end

    private

    def sort_collection
      collection.sort { |e| e.created_at.to_date }
    end

    def format_date(obj)
      obj.created_at.strftime("%d/%m")
    end

    def fetch_normalized_data
      ::BuilderSession::StatusInfo::AbnormalDaysDetection::Detector.call(data: count_data, k: k)
    end

    def fetch_count_data
      sorted_collection.each_with_object({}) do |e, h|
        date = format_date(e)
        h[date] ||= { total: 0 }
        inc_count!(e.summary_status, h[date])
      end
    end

    def inc_count!(status, info)
      info[status.to_sym] = info[status.to_sym].to_i + 1
      info[:total] += 1
    end
  end
end
