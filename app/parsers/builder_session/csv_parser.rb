require 'csv'

module BuilderSession
  class CsvParser
    include Virtus.model

    # Could be IO or String. Can make custom type or validation.
    attribute :file, Object
    attribute :fields, Array, default: []
    attribute :lines, Array[Hash], default: []
    attribute :parsed_data, Array[::BuilderSession::BaseObject], lazy: true, default: :parse_and_wrapp

    def call
      parsed_data
    end

    private

    def parse_and_wrapp
      parse_file
      lines.map { |e| ::BuilderSession::BaseObject.new(e) }
    end

    def parse_file
      ::CSV.foreach(file) { |e| $INPUT_LINE_NUMBER == 1 ? fetch_fields(e) : parse_line(e) }
    end

    def fetch_fields(row_data)
      self.fields = row_data.map { |e| e.parameterize(separator: "_") }
    end

    def parse_line(row_data)
      self.lines << Hash[*fields.zip(row_data).flatten]
    end
  end
end
