Rails.application.routes.draw do
  root 'builder_sessions#statistics'

  post 'parse_file' => 'builder_sessions#parse_file'
end
